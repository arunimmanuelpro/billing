-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 04, 2018 at 01:32 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `billing_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE IF NOT EXISTS `discounts` (
`id` int(50) NOT NULL,
  `userId` int(50) NOT NULL,
  `productCode` varchar(50) NOT NULL,
  `discount` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `userId`, `productCode`, `discount`) VALUES
(2, 1, '456', 0.9),
(3, 1, '12345', 0.8);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE IF NOT EXISTS `invoice` (
`id` int(50) NOT NULL,
  `invoiceId` int(50) NOT NULL,
  `productCode` varchar(50) NOT NULL,
  `qty` float NOT NULL,
  `price` float NOT NULL,
  `discount` float NOT NULL,
  `igstrate` float NOT NULL,
  `igstamount` float NOT NULL,
  `amount` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `invoiceId`, `productCode`, `qty`, `price`, `discount`, `igstrate`, `igstamount`, `amount`) VALUES
(1, 3, '456', 2, 150, 0.5, 10, 29.85, 328.35),
(2, 3, '12345', 1, 115, 0.8, 12, 13.6896, 127.77),
(3, 4, '456', 2, 150, 0.5, 10, 29.85, 328.35),
(4, 4, '12345', 1, 115, 0.8, 12, 13.6896, 127.77),
(5, 5, '456', 2, 150, 0.5, 10, 29.85, 328.35),
(6, 6, '456', 2, 150, 0.5, 10, 29.85, 328.35),
(7, 7, '456', 2, 150, 0.5, 10, 29.85, 328.35),
(8, 8, '456', 2, 150, 0.5, 10, 29.85, 328.35),
(9, 9, '456', 2, 150, 0.5, 10, 29.85, 328.35),
(10, 10, '456', 2, 150, 0.5, 10, 29.85, 328.35),
(11, 11, '456', 2, 150, 0.5, 10, 29.85, 328.35),
(12, 11, '12345', 6, 115, 0.8, 12, 82.1376, 766.618),
(13, 12, '456', 2, 150, 0.5, 10, 29.85, 328.35),
(14, 12, '12345', 6, 115, 0.8, 12, 82.1376, 766.618),
(15, 13, '456', 2, 150, 0.5, 10, 29.85, 328.35),
(16, 13, '12345', 5, 115, 0.8, 12, 68.448, 638.848),
(17, 14, '456', 2, 150, 0.5, 10, 29.85, 328.35),
(18, 14, '12345', 2, 115, 0.8, 12, 27.3792, 255.539),
(19, 15, '456', 2, 150, 0.5, 10, 29.85, 328.35),
(20, 16, '456', 2, 150, 0.5, 10, 29.85, 328.35),
(21, 16, '12345', 1, 115, 0.8, 12, 13.6896, 127.77),
(22, 17, '456', 1, 150, 0.5, 10, 14.925, 164.175),
(23, 17, '12345', 2, 115, 0.8, 12, 27.3792, 255.539),
(24, 18, '456', 1, 150, 0.5, 10, 14.925, 164.175),
(25, 18, '12345', 2, 115, 0.8, 12, 27.3792, 255.539),
(26, 19, '456', 5, 150, 0.5, 10, 74.625, 820.875),
(27, 19, '12345', 10, 115, 0.8, 12, 136.896, 1277.7),
(28, 19, '456', 5, 150, 0.5, 10, 74.625, 820.875),
(29, 19, '12345', 20, 115, 0.8, 12, 273.792, 2555.39),
(30, 19, '456', 10, 150, 0.9, 10, 148.65, 1635.15);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_master`
--

CREATE TABLE IF NOT EXISTS `invoice_master` (
`id` int(50) NOT NULL,
  `userId` int(50) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_master`
--

INSERT INTO `invoice_master` (`id`, `userId`, `timestamp`) VALUES
(1, 1, '2018-04-03 19:20:38'),
(2, 1, '2018-04-03 19:33:26'),
(3, 1, '2018-04-03 19:36:43'),
(4, 1, '2018-04-03 19:47:28'),
(5, 1, '2018-04-03 19:48:45'),
(6, 1, '2018-04-03 19:49:45'),
(7, 1, '2018-04-03 19:50:21'),
(8, 1, '2018-04-03 19:51:06'),
(9, 1, '2018-04-03 19:51:40'),
(10, 1, '2018-04-03 19:52:28'),
(11, 1, '2018-04-03 20:03:03'),
(12, 1, '2018-04-03 20:03:51'),
(13, 1, '2018-04-03 20:06:11'),
(14, 1, '2018-04-03 20:09:35'),
(15, 1, '2018-04-03 20:10:37'),
(16, 1, '2018-04-03 20:11:27'),
(17, 1, '2018-04-03 20:25:47'),
(18, 1, '2018-04-03 20:26:55'),
(19, 1, '2018-04-03 22:39:38');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
`id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `hsn` int(50) NOT NULL,
  `unit` varchar(50) NOT NULL,
  `igst` float NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `code`, `description`, `hsn`, `unit`, `igst`, `price`) VALUES
(1, '12345', 'Test', 5702, 'Pcs', 12, 115),
(2, '8465', 'Sample Test', 6304, 'Metre', 5, 125),
(3, '456', 'sjnkjn', 545, 'Metre', 10, 150),
(4, '457', 'skdbb', 5165, 'Pcs', 5, 55),
(5, '7789', 'Sample', 78, 'Pcs', 25, 45600);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
`id` int(50) NOT NULL,
  `userName` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `userName`, `password`, `name`) VALUES
(1, 'admin', 'Admin@123', 'Administraor');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `state` varchar(50) NOT NULL,
  `gstin` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `address`, `state`, `gstin`) VALUES
(1, 'Arun Jaya Immanuel V', 'Coimbatore	', 'Tamil Nadu', '123456789');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_master`
--
ALTER TABLE `invoice_master`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `invoice_master`
--
ALTER TABLE `invoice_master`
MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
